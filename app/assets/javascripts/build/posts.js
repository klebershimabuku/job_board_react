/** @jsx React.DOM */
var Posts = React.createClass({displayName: 'Posts',
  render: function() {
    return (
      React.DOM.div( {className:"posts"}, 
        React.DOM.h3(null, "Featured Jobs"),
        PostList(null ),
        React.DOM.p(null, "More Awesome Jobs →")
      )
    )
  }
});

var PostList = React.createClass({displayName: 'PostList',
  getInitialState: function() {
    return { posts: [
    ] };
  },

  componentDidMount: function() {
    $.getJSON('/api/posts', function(results) {
      this.setState({
        posts: results
      });
    }.bind(this));
  },

  render: function() {
    var posts = this.state.posts.map(function(post) {
      return PostListItem( {post:post} );
    });

    return React.DOM.ul( {className:"post-list"}, posts);
  }
});

var PostListItem = React.createClass({displayName: 'PostListItem',
  handleClick: function(e) {
    var post_id = this.props.post.id;
    var path = '/api/posts/' + post_id;
    $.getJSON(path, function(post) {
      React.renderComponent(Post(null ), document.getElementById('contents')).setProps(post);
    });
  },

  render: function() {
    return (
      React.DOM.div( {className:"post-item"}, 
        React.DOM.li( {key:this.props.post.id}, React.DOM.a( {href:"#", onClick:this.handleClick}, this.props.post.title))
      )
    );
  }
});

var Post = React.createClass({displayName: 'Post',
  render: function() {
    return(
      React.DOM.div( {className:"post"}, 
        React.DOM.h1(null, this.props.title),
        React.DOM.h2(null, this.props.location),
        React.DOM.p(null, this.props.description)
      )
      );
  }
});
