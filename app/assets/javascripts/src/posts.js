/** @jsx React.DOM */
var Posts = React.createClass({
  render: function() {
    return (
      <div className="posts">
        <h3>Featured Jobs</h3>
        <PostList />
        <p>More Awesome Jobs →</p>
      </div>
    )
  }
});

var PostList = React.createClass({
  getInitialState: function() {
    return { posts: [
    ] };
  },

  componentDidMount: function() {
    $.getJSON('/api/posts', function(results) {
      this.setState({
        posts: results
      });
    }.bind(this));
  },

  render: function() {
    var posts = this.state.posts.map(function(post) {
      return <PostListItem post={post} />;
    });

    return <ul className='post-list'>{posts}</ul>;
  }
});

var PostListItem = React.createClass({
  handleClick: function(e) {
    var post_id = this.props.post.id;
    var path = '/api/posts/' + post_id;
    $.getJSON(path, function(post) {
      React.renderComponent(<Post />, document.getElementById('contents')).setProps(post);
    });
  },

  render: function() {
    return (
      <div className="post-item">
        <li key={this.props.post.id}><a href="#" onClick={this.handleClick}>{this.props.post.title}</a></li>
      </div>
    );
  }
});

var Post = React.createClass({
  render: function() {
    return(
      <div className="post">
        <h1>{this.props.title}</h1>
        <h2>{this.props.location}</h2>
        <p>{this.props.description}</p>
      </div>
      );
  }
});
