class Api::PostsController < ApplicationController
  respond_to :json

  def index
    respond_with(Post.all.order('id desc'))
  end

  def show
    post = Post.where(id: params[:id]).first
    respond_with(post)
  end
end
